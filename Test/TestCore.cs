﻿using System;
using it.unibo.biscia.core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace it.unibo.biscia.test.fiorentini
{
    [TestClass]
    public class TestCore
    {
        [TestMethod]
        public void TestCell()
        {
            FactoryForInternal f = new FactoryForInternal();
            ICell cell;
            for (int c=0; c < 100;c++)
            {
                for (int r = 0; r < 100; r++)
                {
                    cell = f.CreateCell(c, r);
                    Assert.AreEqual(c, cell.Col, String.Format("Column of cell {0}", cell));
                    Assert.AreEqual(r, cell.Row, String.Format("Row of cell {0}", cell));
                    ICell cell2 = f.CreateCell(c, r);
                    Assert.AreNotSame(cell2, cell, "Different instances equals are different");
                    Assert.AreEqual(cell2, cell, String.Format("Not equals cells with same coords {0} and {1}", c, r));
                }
            }
        }

        [TestMethod]
        public void TestLevel()
        {
            FactoryForInternal f = new FactoryForInternal();
            ILevel level;
            for (int c = 1; c < 10; c++)
            {
                for (int r = 1; r < 10; r++)
                {
                    for (int car = 0; car < 10; car++)
                    {
                        level = f.CreateLevel(c, r, car);
                        Assert.AreEqual(c, level.Cols, "Columns of level");
                        Assert.AreEqual(r, level.Rows, "Rows of level");
                        Assert.AreEqual(car, level.Cardinal, "Level cardinal");
                        Assert.AreEqual(0, level.Entities.Count, "Not implemented entity aro allwayas zero");
                    }
                }
            }
        }

        [TestMethod]
        public void TestDirection()
        {
            IDirection u = Direction.UP;
            IDirection d = Direction.DOWN;
            IDirection l = Direction.LEFT;
            IDirection r = Direction.RIGHT;
            Assert.AreEqual(0, u.StepCol, String.Format("StepCol {0}", u));
            Assert.AreEqual(-1, u.StepRow, String.Format("StepRow {0}", u));
            Assert.AreEqual(0, d.StepCol, String.Format("StepCol {0}", d));
            Assert.AreEqual(1, d.StepRow, String.Format("StepRow {0}", d));
            Assert.AreEqual(-1, l.StepCol, String.Format("StepCol {0}", l));
            Assert.AreEqual(0, l.StepRow, String.Format("StepRow {0}", l));
            Assert.AreEqual(1, r.StepCol, String.Format("StepCol {0}", r));
            Assert.AreEqual(0, r.StepRow, String.Format("StepRow {0}", r));

        }
    }
}
