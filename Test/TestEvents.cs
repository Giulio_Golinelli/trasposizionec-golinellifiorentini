﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using it.unibo.biscia.events;
using it.unibo.biscia.utils;
using it.unibo.biscia.core;

namespace it.unibo.biscia.test.golinelli
{
    /// <summary>
    /// A test class to verify Events packaage
    /// </summary>
    [TestClass]
    public class TestEvents
    { 
        /// <summary>
        /// A test method to verify Events package
        /// </summary>
        [TestMethod]
        public void EventsTest()
        {
            //binding subject and observer
            IGenericEventSubject<IActionObserver> actionSubject = new GenericEventSubject<IActionObserver>();
            ActionObserver actionObserver = new ActionObserver();
            actionSubject.Attach(actionObserver);

            //changing of a player is actually comunicated
            Pair<Player, IDirection> mockPlayer = new Pair<Player, IDirection>(new Player("Player1"), Direction.UP);
            actionSubject.Notify((a) => a.Move(mockPlayer.first, mockPlayer.second));
            Assert.AreEqual(mockPlayer, actionObserver.currentPlayerDirection);
            mockPlayer = new Pair< Player, IDirection > (new Player("Player2"), Direction.DOWN);
            actionSubject.Notify((a) => a.Move(mockPlayer.first, mockPlayer.second));
            Assert.AreEqual(mockPlayer, actionObserver.currentPlayerDirection);
            
            // checking if pausing and resuming works
            actionSubject.Notify((a) => a.PauseAndResume());
            Assert.IsTrue(actionObserver.isPaused);
            actionSubject.Notify((a) => a.PauseAndResume());
            Assert.IsFalse(actionObserver.isPaused);

            //checking if Detach and Attach works
            actionSubject.Detach(actionObserver);
            actionSubject.Notify((a) => a.End());
            Assert.IsFalse(actionObserver.isEnded);
            actionSubject.Attach(actionObserver);
            actionSubject.Notify((a) => a.End());
            Assert.IsTrue(actionObserver.isEnded);
        }
    }
}
