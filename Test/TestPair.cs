﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using it.unibo.biscia.utils;

namespace it.unibo.biscia.test.golinelli
{
    [TestClass]
    public class TestPair
    {
        [TestMethod]
        public void PairTest()
        {
            Pair<string, int> p1 = new Pair<string, int>("chiave-valore", 1);
            Assert.AreEqual(p1.first, "chiave-valore");
            Assert.AreNotEqual(p1.second, "valore-chiave");
            Pair<string, int> p2 = new Pair<string, int>("chiave", 1);
            Assert.AreNotEqual(p1, p2);
            Assert.AreSame(p1, p1);
        }
    }
}
