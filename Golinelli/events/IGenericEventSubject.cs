﻿using System;

namespace it.unibo.biscia.events
{
    /// <summary>
    /// A Subject to notify actions to all listeners.
    /// </summary>
    /// <typeparam name="T">The type of listener</typeparam>
    public interface IGenericEventSubject<T> : IGenericObserverCollector<T>
    {
        /// <summary>
        /// Perform an action to any listener registered.
        /// </summary>
        /// <param name="actionable">The action to be performed</param>
        void Notify(Action<T> actionable);
    }
}
