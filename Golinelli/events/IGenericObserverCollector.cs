﻿namespace it.unibo.biscia.events
{
    /// <summary>
    /// For register and remove observer of events.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericObserverCollector<T>
    {
        /// <summary>
        /// Attaches a listener.
        /// </summary>
        /// <param name="observer">The listener to be attached.</param>
        void Attach(T observer);

        /// <summary>
        /// Detaches a listener.
        /// </summary>
        /// <param name="observer">The listener to be detached.</param>
        void Detach(T observer);
    }
}

