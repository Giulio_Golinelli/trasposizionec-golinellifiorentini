﻿using System;
using System.Collections.Generic;

namespace it.unibo.biscia.events
{
    /// <summary>
    /// Implements a simple notifier of events.
    /// </summary>
    /// <typeparam name="T">object type to consume on notify</typeparam>
    public class GenericEventSubject<T> : IGenericEventSubject<T>
    {
        private readonly HashSet<T> observers;

        public GenericEventSubject()
        {
            this.observers = new HashSet<T>();
        }

        public void Attach(T observer)
        {
            lock (observers)
            {
                observers.Add(observer);
            }
        }

        public void Detach(T observer)
        {
            lock (observer)
            {
                observers.Remove(observer);
            }
        }

        public void Notify(Action<T> actionable)
        {
            HashSet<T> newObservers;
            lock (observers)
            {
                newObservers = new HashSet<T>(observers);
            }
            foreach(T observer in newObservers)
            {
                try
                {
                    actionable.Invoke(observer);
                }
                catch (InvalidOperationException ioe)
                {
                    Console.WriteLine($"Unsupported operation exception: '{ioe}'");
                }
            }
        }
    }
}
