﻿using System;
using it.unibo.biscia.core;
using it.unibo.biscia.utils;

namespace it.unibo.biscia.events
{
    /// <summary>
    /// A mock implementation for test usage of IActionObserver
    /// </summary>
    public class ActionObserver : IActionObserver
    {
        /// <summary>
        /// Current Player and Direction.
        /// </summary>
        public Pair<Player, IDirection> currentPlayerDirection { get; private set; }

        /// <summary>
        ///  if the game is paused.
        /// </summary>
        /// 
        public bool isPaused { get; private set; }
        
        /// <summary>
        /// If the game is ended.
        /// </summary>
        public bool isEnded { get; private set; }

        public ActionObserver()
        {
            this.isEnded = false;
            this.isPaused = false;
        }

        public void End()
        {
            isEnded = !isEnded;
        }

        public void Move(Player player, IDirection direction)
        {
            currentPlayerDirection = new Pair<Player, IDirection>(player, direction);
        }

        public void PauseAndResume()
        {
            isPaused = !isPaused;
        }
    }
}
