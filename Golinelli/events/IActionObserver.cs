﻿using it.unibo.biscia.core;

namespace it.unibo.biscia.events
{
    /// <summary>
    /// Observer for player's actions.
    /// </summary>
    public interface IActionObserver
    {
        /// <summary>
        /// A player has moved in a direction.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="direction"></param>
        void Move(Player player, IDirection direction);

        /// <summary>
        /// The game has been paused or resumed.
        /// </summary>
        void PauseAndResume();

        /// <summary>
        /// The game has ended.
        /// </summary>
        void End();
    }
}
