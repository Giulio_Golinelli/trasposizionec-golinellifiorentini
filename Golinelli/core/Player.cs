﻿namespace it.unibo.biscia.core
{
    /// <summary>
    /// A player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Initial player's lives.
        /// </summary>
        private readonly static int INITIAL_LIVES = 5;

        /// <summary>
        /// Player's name.
        /// </summary>
        public string name { get; }
        /// <summary>
        /// Player's lives.
        /// </summary>
        public int lives { get { return INITIAL_LIVES; } }

        public Player(string name)
        {
            this.name = name;
        }
    }
}
