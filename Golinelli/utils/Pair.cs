﻿namespace it.unibo.biscia.utils
{
    /// <summary>
    /// Plain Old CLR Object for pairing two different objects.
    /// </summary>
    /// <typeparam name="X">First object type</typeparam>
    /// <typeparam name="Y">Second object type</typeparam>
    public class Pair<X, Y>
    {
        public X first { get; }
        public Y second { get; }

        public Pair(X first, Y second)
        {
            this.first = first;
            this.second = second;
        }

        public bool Equals(Pair<X, Y> other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(first, other.first) && string.Equals(second, other.second);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != this.GetType())
                return false;
            return Equals((Pair<X, Y>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((first != null ? first.GetHashCode() : 0) * 397) ^ (second != null ? second.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return "Pair [first=" + first.ToString() + ", second=" + second.ToString() + "]";
        }
    }
}
