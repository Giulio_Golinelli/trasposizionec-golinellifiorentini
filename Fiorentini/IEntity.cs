﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    public enum EntityType
    {
        /**
         * if biscia crush over this, die.
         */
        WALL,
        /**
         * if biscia crush over this, eat.
         */
        FOOD,
        /**
         * principal friend of players.
         */
        SNAKE
    }

    public interface IEntity
    {
        IList<ICell> Cells { get; }
        EntityType Type { get; }
    }
}
