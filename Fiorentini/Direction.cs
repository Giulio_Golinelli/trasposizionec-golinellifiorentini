﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    public class Direction : IDirection
    {
        //https://stackoverflow.com/questions/469287/c-sharp-vs-java-enum-for-those-new-to-c
        public static readonly IDirection UP = new Direction(0, -1);
        public static readonly IDirection DOWN = new Direction(0, 1);
        public static readonly IDirection LEFT = new Direction(-1, 0);
        public static readonly IDirection RIGHT = new Direction(1, 0);

        public static IEnumerable<IDirection> Values
        {
            get
            {
                yield return UP;
                yield return DOWN;
                yield return LEFT;
                yield return RIGHT;
            }
        }
        private readonly int _stepCol;
        private readonly int _stepRow;
        private Direction(int stepCol, int stepRow)
        {
            this._stepCol = stepCol;
            this._stepRow = stepRow;
        }

        public int StepCol { get { return _stepCol; } }

        public int StepRow { get { return _stepRow; } }

        public override string ToString()
        {
            if (ReferenceEquals(this, DOWN))
            {
                return "D";
            }
            if (ReferenceEquals(this, UP))
            {
                return "U";
            }
            if (ReferenceEquals(this, LEFT))
            {
                return "L";
            }
            if (ReferenceEquals(this, RIGHT))
            {
                return "R";
            }
            return "";

        }
    }
}
