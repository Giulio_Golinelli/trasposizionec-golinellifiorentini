﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace it.unibo.biscia.core
{
    public class FactoryForInternal
    {
        public ICell CreateCell(int col, int row)
        {
            return (ICell) new SmartCellImpl(col, row);
        }

        public ILevel CreateLevel(int cols, int rows, int cardinal)
        {
            ILevelManaged level = new LevelImpl(cols, rows)
            {
                Cardinal = cardinal
            };
            return (ILevel) level;
        }
    }
}
