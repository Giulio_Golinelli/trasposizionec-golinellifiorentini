﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    internal interface IEntityManaged
    {
        IList<ISmartCell> SmartCells { get; }

        bool RemoveCell(int Index);

        int RemoveFromCell(int Index);
    }
}
