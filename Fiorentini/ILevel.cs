﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    public interface ILevel
    {
        //Spostate nella classe LevelImpl a seguito di incompatibilità con versione c# usata
        //Errore CS8370  La funzionalità 'implementazione di interfaccia predefinita' non è disponibile in C# 7.3. Usare la versione 8.0 o versioni successive del linguaggio.
        //public const int MIN_COLS = 80;
        //public const int MIN_ROWS = 50;
        int Cardinal { get; }
        IList<IEntity> Entities { get; }
        int Cols { get; }
        int Rows { get; }
    }
}
