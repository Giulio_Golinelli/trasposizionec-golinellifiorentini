﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    public interface IDirection
    {
        int StepCol { get; }
        int StepRow { get; }
    }
}
