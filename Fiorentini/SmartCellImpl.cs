﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    internal class SmartCellImpl : ISmartCell
    {
        private readonly int _col;
        private readonly int _row;
        private String _toStringCache;
        private readonly int _hashCodeCache;
        private readonly ILevelManaged _level = null;
        internal SmartCellImpl(int col, int row)
        {
            if (col < 0 || row < 0)
            {
                throw new ArgumentException();
            }
            _col = col;
            _row = row;
            _hashCodeCache = this.CalcHashCode();
        }
        internal SmartCellImpl(ILevelManaged level, int col, int row) : this(col, row)
        {
            _level = level;
        }

        int ICell.Col
        {
            get
            {
                return _col;
            }
        }
        int ICell.Row
        {
            get
            {
                return _row;
            }
        }

        ISmartCell ISmartCell.GeSideCell(IDirection direction)
        {
            //for compatiblity whith older versione of c#
#pragma warning disable IDE0041 // Usa controllo 'is null'
            if (ReferenceEquals(_level, null))
#pragma warning restore IDE0041 // Usa controllo 'is null'
            {
                int col = _col + direction.StepCol;
                int row = _row + direction.StepRow;
                if (col < 0)
                {
                    col = LevelImpl.MIN_COLS;
                }
                if (row < 0)
                {
                    row = LevelImpl.MIN_ROWS;
                }
                return new SmartCellImpl(col, row);
            }
            return _level.GetSideCell(this, direction);

        }
        ISet<IEntityManaged> ISmartCell.GetContainedEntities()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            //for compatiblity whith older versione of c#
#pragma warning disable IDE0041 // Usa controllo 'is null'
            if (ReferenceEquals(obj,null))
#pragma warning restore IDE0041 // Usa controllo 'is null'
            {
                return false;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            ISmartCell other = (ISmartCell)obj;
            if (_col != other.Col)
            {
                return false;
            }
            return _row == other.Row;

        }

        public override int GetHashCode()
        {
            return this._hashCodeCache;
        }

        public override string ToString()
        {
            //for compatiblity whith older versione of c#
#pragma warning disable IDE0041 // Usa controllo 'is null'
            if (ReferenceEquals(this._toStringCache, null))
#pragma warning restore IDE0041 // Usa controllo 'is null'
            {
                this._toStringCache = "[" + _col + ", " + _row + "]";
            }

            return this._toStringCache;
        }

        private int CalcHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + _col;
            result = prime * result + _row;
            return result;
        }

    }
}
