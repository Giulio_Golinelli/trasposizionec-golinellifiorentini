﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace it.unibo.biscia.core
{
   [TestClass]
   public class Test
    {
        [TestMethod]
        public void TestCell_Creating()
        {
            //test  for smartCell

            SmartCellImpl c1;
            //test for valid smartCell public constructor
            try
            {
                c1 = new SmartCellImpl(0, 0);
            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Unexpected error {0} for valid coord creating SmartCellImpl", e));
            }
            //test for wrong smartCell public constructor
            try
            {
                c1 = new SmartCellImpl(-1, -1);
                Assert.Fail("Not error for coord negative creating SmartCellImpl");
            }
            catch (ArgumentException)
            {
                //ok
            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Unexpected error {0} for coord negative creating SmartCellImpl", e));
            }
        }
        [TestMethod]
        public void TestCell_Col_Row_GetSideCell()
        {
            //test  for smartCell

            ISmartCell c1;
            ISmartCell c2;
            ISmartCell c3;
            ISmartCell c4;
            ISmartCell c5;
            //test for smartCell.Col & smartCell.Row & smartCell.GeSideCell
            for (int c = 1; c < 100; c++)
            {
                for (int r = 1; r < 100; r++)
                {
                    c1 = new SmartCellImpl(c, r);
                    VerifyCellCoords("new SmartCellImpl", c, r, c1);
                    c2 = c1.GeSideCell(Direction.RIGHT);
                    VerifyCellCoords("GeSideCell RIGHT", c + 1, r, c2);
                    c3 = c2.GeSideCell(Direction.DOWN);
                    VerifyCellCoords("GeSideCell DOWN", c + 1, r + 1, c3);
                    c4 = c3.GeSideCell(Direction.LEFT);
                    VerifyCellCoords("GeSideCell LEFT", c, r + 1, c4);
                    c5 = c4.GeSideCell(Direction.UP);
                    VerifyCellCoords("GeSideCell UP", c, r, c5);
                    Assert.AreEqual(c1, c5, "Loop start and end");
                    Assert.IsTrue(c1.Equals(c5), "Loop start equals end");
                }
            }
        }

        private static void VerifyCellCoords(String message, int expectedCol, int expectedRow, ICell cell)
        {
            Assert.AreEqual(expectedCol, cell.Col, message + " Col");
            Assert.AreEqual(expectedRow, cell.Row, message + " Row");
        }

        [TestMethod]
        public void TestLevel_Creating()
        {
            //test for LevelImp Constructor
            ILevelManaged l;
            l = new LevelImpl(); //default dimensions
            Assert.AreEqual(LevelImpl.MIN_COLS, l.Cols, "Level columns");
            Assert.AreEqual(LevelImpl.MIN_ROWS, l.Rows, "Level rows");
            for (int c = 1; c < 10; c++)
            {
                for (int r = 1; r < 10; r++)
                {
                    l = new LevelImpl(c, r);
                    Assert.AreEqual(c, l.Cols, "Level columns");
                    Assert.AreEqual(r, l.Rows, "Level rows");
                }

            }
        }

        [TestMethod]
        public void TestLevel_Cardinal()
        {

            ILevelManaged l = new LevelImpl(15, 10);
            //l.Cardinal
            //l.SetCardinal
            //test for LevelImp (Cardinal & SetCardinal)
            Assert.AreEqual(0, l.Cardinal, "LevelImpl.Cardinal default");
            for (int c = 0; c < 20; c++)
            {
                l.Cardinal = c;
                Assert.AreEqual(c, l.Cardinal, "LevelImpl.SetCardinal");
            }
        }

        [TestMethod]
        public void TestLevel_Cells_Cols_Rows_GetCell_GetSideCell()
        {
            //l.Cells
            //l.Cols
            //l.Rows
            //l.GetCell
            //l.GetSideCell
            //test for LevelImp (Cells, Cols, Rows, GetCell, GetSideCell)"
            ISmartCell c1;
            ISmartCell c2;
            ISmartCell c3;
            ILevelManaged l = new LevelImpl(15, 10);
            Assert.AreEqual(l.Cols * l.Rows, l.Cells.Count, "Cells count ");
            for (int c = 0; c < l.Cols; c++)
            {
                IDirection d = Direction.UP;
                c1 = l[c, 0];
                c2 = l.GetSideCell(c1, d);
                c3 = c1.GeSideCell(d);
                Assert.AreEqual(c1.Col, c2.Col, d.ToString() + " cell as same col");
                Assert.AreEqual(l.Rows - 1, c2.Row, d.ToString() + " cell is at other side");
                Assert.IsTrue(c2.Equals(c3), d.ToString() + " cell for level is equals " + d.ToString() + " cell for cell");
                d = Direction.DOWN;
                c1 = l[c, l.Rows - 1];
                c2 = l.GetSideCell(c1, d);
                c3 = c1.GeSideCell(d);
                Assert.AreEqual(c1.Col, c2.Col, d.ToString() + " cell as same col");
                Assert.AreEqual(0, c2.Row, d.ToString() + " cell is at other side");
                Assert.IsTrue(c2.Equals(c3), d.ToString() + " cell for level is equals " + d.ToString() + " cell for cell");
            }
            for (int r = 0; r < l.Rows; r++)
            {
                IDirection d = Direction.LEFT;
                c1 = l[0, r];
                c2 = l.GetSideCell(c1, d);
                c3 = c1.GeSideCell(d);
                Assert.AreEqual(c1.Row, c2.Row, d.ToString() + " cell as same Row");
                Assert.AreEqual(l.Cols - 1, c2.Col, d.ToString() + " cell is at other side");
                Assert.IsTrue(c2.Equals(c3), d.ToString() + " cell for level is equals " + d.ToString() + " cell for cell");
                d = Direction.RIGHT;
                c1 = l[l.Cols - 1, r];
                c2 = l.GetSideCell(c1, d);
                c3 = c1.GeSideCell(d);
                Assert.AreEqual(c1.Row, c2.Row, d.ToString() + " cell as same Row");
                Assert.AreEqual(0, c2.Col, d.ToString() + " cell is at other side");
                Assert.IsTrue(c2.Equals(c3), d.ToString() + " cell for level is equals " + d.ToString() + " cell for cell");
            }
            foreach (var cell in l.Cells)
            {
                foreach (IDirection d in Direction.Values)
                {
                    int max;
                    if (d.Equals(Direction.UP) || d.Equals(Direction.DOWN))
                    {
                        max = l.Rows;
                    }
                    else
                    {
                        max = l.Cols;
                    }
                    var cell2 = cell;
                    for (int c = 0; c < max; c++)
                    {
                        var cell3 = cell2;
                        cell2 = l.GetSideCell(cell2.Col, cell2.Row, d);
                        Assert.IsFalse(cell2.Equals(cell3), cell3.ToString() + " after movement " + d.ToString() + " is not equals to " + cell2.ToString());
                    }
                    VerifyCellCoords(cell.ToString() + " after turn around " + d.ToString(), cell.Col, cell.Row, cell2);

                }

            }
        }

        [TestMethod]
        public void TestLevel_Area()
        {
            ILevelManaged l = new LevelImpl(15, 10);
            //size < 0
            var area = l.GetArea(l[2, 2], -2, -2);
            var cell = l[1, 1];
            Assert.IsTrue(area.Contains(cell), "Area " + area.ToString() + " not include cell " + cell.ToString());
            cell = l[1, 2];
            Assert.IsTrue(area.Contains(cell), "Area " + area.ToString() + " not include cell " + cell.ToString());
            cell = l[2, 1];
            Assert.IsTrue(area.Contains(cell), "Area " + area.ToString() + " not include cell " + cell.ToString());
            cell = l[2, 2];
            Assert.IsTrue(area.Contains(cell), "Area " + area.ToString() + " not include cell " + cell.ToString());
            Assert.AreEqual(4, area.Count, "Area " + -2 + "*" + -2 + " Size");
            try
            {
                l.GetArea(l[0, 0], l.Cols+1, 1);
                Assert.Fail("Not error for over max size on getArea");
            }
            catch (ArgumentException)
            {
                //ok
            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Unexpected error {0} for for over max size on getArea", e));
            }


            for (int width = 0; width <= l.Cols; width++)
            {
                for (int heigth = 0; heigth <= l.Rows; heigth++)
                {
                    foreach (var startcell in l.Cells)
                    {
                        area = l.GetArea(startcell, width, heigth);
                        Assert.AreEqual(width * heigth, area.Count, "Area " + startcell.ToString() + ">" + width + "*" + heigth + " Size");
                        for (int c = 0; c < width; c++)
                        {
                            for (int r = 0; r < heigth; r++)
                            {
                                cell = l[c + startcell.Col, r + startcell.Row];
                                Assert.IsTrue(area.Contains(cell), "Area " + startcell.ToString() + ">" + width + "*" + heigth + " not include cell " + cell.ToString());
                            }
                        }

                    }

                }
            }
            for (int startc = 0; startc < l.Cols; startc++)
            {
                for (int startr = 0; startr < l.Rows; startr++)
                {
                    for (int endc = startc; endc < l.Cols; endc++)
                    {
                        for (int endr = startr; endr < l.Rows; endr++)
                        {
                            var startcell = l[startc, startr];
                            var endcell = l[endc, endr];
                            area = l.GetArea(startcell, endcell);
                            Assert.AreEqual((endc - startc + 1) * (endr - startr + 1), area.Count, "Area " + startcell.ToString() + ":" + endcell.ToString() + " Size");
                            for (int c = startc; c <= endc; c++)
                            {
                                for (int r = startr; r <= endr; r++)
                                {
                                    cell = l[c, r];
                                    Assert.IsTrue(area.Contains(cell), "Area " + startcell.ToString() + ":" + endcell.ToString() + " not include cell " + cell.ToString());
                                }
                            }

                        }
                    }
                }
            }
        }
    }
}
