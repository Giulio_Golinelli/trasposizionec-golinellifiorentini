﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    public interface ICell
    {

        int Col { get; }
        int Row { get; }
    }
}
