﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{

    internal interface ISmartCell: ICell
    {
        ISmartCell GeSideCell(IDirection direction);
        ISet<IEntityManaged> GetContainedEntities();
    }
}
