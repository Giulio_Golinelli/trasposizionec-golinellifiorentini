﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace it.unibo.biscia.core
{
    internal class LevelImpl : ILevelManaged
    {
        //Spostate dall'interfaccia ILevel a seguito di incompatibilità con versione c# usata
        //Errore CS8370  La funzionalità 'implementazione di interfaccia predefinita' non è disponibile in C# 7.3. Usare la versione 8.0 o versioni successive del linguaggio.
        internal const int MIN_COLS = 80;
        internal const int MIN_ROWS = 50;

        private int _level;
        private readonly IList<IList<ISmartCell>> _cells;

        private readonly int _cols;
        private readonly int _rows;
        private int _cardinal;
        private readonly ISet<IEntityManaged> _entities = new HashSet<IEntityManaged>();
        /**
         * basic constructor for empty board at min size.
         */
        internal LevelImpl(): this(LevelImpl.MIN_COLS, LevelImpl.MIN_ROWS) {}
        internal LevelImpl(int cols, int rows)
        {
            if (cols <= 0 || rows <= 0)
            {
                throw new ArgumentException();
            }
            this._cols = cols;
            this._rows = rows;
            this._level = 0;
            this._cells = Enumerable.Range(0, _cols).ToList().Select(c => Enumerable.Range(0, _rows).ToList().Select(r => new SmartCellImpl(this, c, r)).ToList<ISmartCell>()).ToList<IList< ISmartCell>>();
        }
        int ILevel.Cols { get { return _cols; } }
        int ILevel.Rows { get { return _rows; } }
        private int RotateCoord(int coord, int max)
        {
            int c = coord;
            while (c < 0)
            {
                c += max;
            }
            return c % max;
        }
        protected int RotateRow(int row)
        {
            return this.RotateCoord(row, _rows);
        }
        protected int RotateCol(int col)
        {
            return this.RotateCoord(col, _cols);
        }
        
        IList<IEntity> ILevel.Entities
        {
            get
            {
                return new ReadOnlyCollection<IEntity>(_entities.Select(e => (IEntity) e).ToList());
            }
        }

        IList<IEntityManaged> ILevelManaged.EntitiesManaged
        {
            get
            {
                lock(this._entities) {
                    return new ReadOnlyCollection<IEntityManaged>(_entities.ToList());
                }
            }
        }
        protected int Level
        {
            set
            {
                this._level = value;
            }
        }
        void ILevelManaged.AddEntity(IEntityManaged entity)
        {
            foreach (var e in _entities)
            {
                foreach(var c in e.SmartCells)
                {
                    if (entity.SmartCells.Contains(c))
                    {
                        throw new ArgumentException();
                    }
                }
            }
            lock (_entities)
            {
                _entities.Add(entity);
            }
        }
        public override string ToString()
        {
            return "Board [level=" + this._level + ", Cols=" + this._cols + ", Rows=" + this._rows + " Entities="
                        + this._entities + "]";
        }

        IList<ISmartCell> ILevelManaged.Cells
        {
            get
            {
                lock(_cells)
                {
                    return _cells.SelectMany(l => l.Select(e => e)).ToList();
                }
            }
        }

        public int Cardinal
        {
            get { return _cardinal; }
            set { _cardinal = value; }
        }
        /*
        ISmartCell ILevelManaged.GetCell(int col, int row)
        {
            return this[col, row];
        }
        */
        public ISmartCell this[int col, int row]
        {
            get {
                return _cells[RotateCol(col)][RotateRow(row)];
            }
        }
        void ILevelManaged.RemoveEntity(IEntityManaged entity)
        {
            lock(_entities)
            {
                _entities.Remove(entity);
            }
        }
        ISmartCell ILevelManaged.GetSideCell(int col, int row, IDirection direction)
        {
            return ((ILevelManaged) this)[col + direction.StepCol,row + direction.StepRow];
        }
        ISmartCell ILevelManaged.GetSideCell(ISmartCell cell, IDirection direction)
        {
            return ((ILevelManaged)this).GetSideCell(cell.Col, cell.Row, direction);
        }
        IList<ISmartCell> ILevelManaged.GetArea(ISmartCell cell, int width, int height)
        {
            int w = Math.Abs(width);
            int h = Math.Abs(height);
            if (w > this._cols || h > this._rows )
            {
                throw new ArgumentException();
            }
            IList<ISmartCell> ret = new List<ISmartCell>();
            if (width!=0 && height != 0) 
            {
                ISmartCell c1 = cell;
                ISmartCell c2;
                for (int i=1;i<=w;i++)
                {
                    if (i>1)
                    {
                        c1 = ((ILevelManaged)this).GetSideCell(c1, width > 0 ? Direction.RIGHT : Direction.LEFT);
                    }
                    ret.Add(c1);
                    c2 = c1;
                    for (int j = 1; j < h; j++)
                    {
                        c2 = ((ILevelManaged)this).GetSideCell(c2, height > 0 ? Direction.DOWN : Direction.UP);
                        ret.Add(c2);
                    }
                }
            }
            return new ReadOnlyCollection<ISmartCell>(ret);
        }

        IList<ISmartCell> ILevelManaged.GetArea(ISmartCell cell1, ISmartCell cell2)
        {
            return ((ILevelManaged) this).GetArea(cell1, cell2.Col - cell1.Col + 1, cell2.Row - cell1.Row + 1);
        }

    }
}
