﻿using System;
using System.Collections.Generic;
using System.Text;

namespace it.unibo.biscia.core
{
    internal interface ILevelManaged : ILevel
    {
        new int Cardinal { get; set; }
        IList<IEntityManaged> EntitiesManaged { get; }
        //from method to indexer
        ISmartCell this[int col, int row] {get;}
        ISmartCell GetSideCell(int col, int row, IDirection direction);
        ISmartCell GetSideCell(ISmartCell cell, IDirection direction);
        IList<ISmartCell> Cells { get; }
        IList<ISmartCell> GetArea(ISmartCell cell, int width, int height);
        IList<ISmartCell> GetArea(ISmartCell cell1, ISmartCell cell2);
        void AddEntity(IEntityManaged entity);
        void RemoveEntity(IEntityManaged entity);
    }
}
